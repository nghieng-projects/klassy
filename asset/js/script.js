function showNav(event) {
    var navHeader = document.getElementsByClassName('nav-header')[0]||null;
    navHeader.classList.toggle('show');


    var navBtn = event.currentTarget;
    console.log(event.currentTarget)
    navBtn.classList.toggle('active')
}





/// ourchefs
function getApiChefs() {
    fetch('http://localhost:3000/our-chef')
        .then(function (response) {
            return response.json()
        })
        .then(function (chefs) {
            var ourChefs = [];
            chefs.map(function (chef) {
                var html = `
               <div class="box">
                <img src="${chef.img}" alt="">
                <div class="icon">
                    <a href="#"><i class="fab fa-facebook-f"></i></a>
                    <a href="#"><i class="fab fa-twitter"></i></a>
                    <a href="#"><i class="fab fa-instagram"></i></a>
                </div>
                <div class="infoChefs">
                    <h3>${chef.name}</h3>
                    <p>${chef.position}</p>
                </div>
            </div>
                `
                ourChefs.push(html)
            })
            document.getElementById('colChef').innerHTML = ourChefs.join('')
        })
}

getApiChefs()

//klassy menu - near footer
function getMenuKlassy() {
    fetch('http://localhost:3000/klassy-menu')
        .then(function (response) {
            return response.json()
        })
        .then(function (listmenu) {
            console.log(listmenu)
            var klassyMenu = [];
            listmenu.map(function (menu) {
                var htmls = `
               
                   <div class="menu">
                        <img src="${menu.img}" alt="">
                    <div class="name-menu">
                        <h3>${menu.name}</h3>
                        <p>${menu.description}</p>
                    </div>
                    <span>${menu.price}</span>
                   </div>
               
                    `
                klassyMenu.push(htmls);
            })
            document.getElementById('footerMenu').innerHTML = klassyMenu.join('');
        })
}

getMenuKlassy()


// scroll section
$(document).ready(function () {
    //$(document).on("scroll", onScroll);

    // Click vô thẻ a nào có href bắt đầu bằng dấu # thì sẽ chạy hàm bên trong
    $('.scroll-to-section a[href^="#"]').on('click', function (e) {
        e.preventDefault();
        // Ko cần care cái này
        $(document).off("scroll");

        // Thử console.log target ra xem nó có giá trị gì
        var target = this.hash,

            menu = target;

        var target = $(this.hash);

        // Chổ này thử google từ khoá jquery animate xem để hiểu
        // Google thêm từ khoá javascript scrollTop
        $('html, body').stop().animate({
            scrollTop: (target.offset().top) - 79
        }, 500, 'swing', function () {
            window.location.hash = target;
            //$(document).on("scroll", onScroll);
        });

    });
});